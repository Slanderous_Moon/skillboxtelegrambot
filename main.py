from chatbot.bot import SkillboxTestChatbot


if __name__ == '__main__':
    try:
        test_chatbot = SkillboxTestChatbot()
        test_chatbot.run()
    except KeyboardInterrupt:
        exit()
