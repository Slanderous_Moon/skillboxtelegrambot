import os
from setuptools import setup, find_packages


REQUIREMENTS = []
with open('requirements.txt') as f:
    for line in f:
        requirement = line.strip()
        REQUIREMENTS.append(requirement)

DEPENDENCY_LINKS = []
CURRENT_DIRECTORY = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(CURRENT_DIRECTORY, 'README.md')) as readme_file:
    README = readme_file.read()

SETUP_CONFIG = {
    'name': 'Telegram_Chatbot_Skillbox',
    'version': '0.0.1',
    'description': 'Telegram chatbot prototype.',
    'long_description': README,
    'classifiers': [
        "Programming Language :: Python"
    ],
    'author': 'Slanderous_Moon',
    'author_email': 'SlanderousMoon@yandex.ru',
    'packages': find_packages(),
    'include_package_data': True,
    'zip_safe': False,
    'extras_require': {},
    'install_requires': REQUIREMENTS,
    # 'dependency_links': DEPENDENCY_LINKS
}

setup(**SETUP_CONFIG)
