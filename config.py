import os
import json


BOT_TOKEN = os.environ["BOT_TOKEN"]
TELEGRAM_API_URL = "https://api.telegram.org/bot{token}/"

ENGLISH_ALPHABET = 'qwertyuiopasdfghjklzxcvbnm'
RUSSIAN_ALPHABET = 'йцукенгшщзхъфывапролджэячсмитьбюё'
OTHER_SYMBOLS = "- "
ALLOWED_SYMBOLS = ENGLISH_ALPHABET + RUSSIAN_ALPHABET + OTHER_SYMBOLS
MAX_LEVENSTEIN_DISTANCE = 0.2

with open('phrasebook.json') as phrasebook_file:
    PHRASEBOOK = json.load(phrasebook_file)
