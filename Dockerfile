FROM python:3.7.5-buster

RUN mkdir -p /app
WORKDIR /app
COPY . /app
RUN pip3 install -e .

CMD ["python", "-u", "main.py"]