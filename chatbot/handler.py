import requests

from config import TELEGRAM_API_URL


# Telegram API methods
GET_UPDATES_METHOD = 'getUpdates'
SEND_MESSAGE_METHOD = 'sendMessage'


class TelegramBotHandler:

    def __init__(self, token):
        self._token = token
        self._api_url = TELEGRAM_API_URL.format(token=token)

    def get_updates(self, offset=None, timeout=100):
        params = {'timeout': timeout, 'offset': offset}
        response = requests.get(
            self._api_url + GET_UPDATES_METHOD,
            params
        )
        result_json = response.json()['result']
        return result_json

    def send_message(self, chat_id, text):
        params = {'chat_id': chat_id, 'text': text}
        response = requests.post(
            self._api_url + SEND_MESSAGE_METHOD,
            params
        )
        return response
