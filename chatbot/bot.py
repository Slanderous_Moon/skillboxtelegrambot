from config import BOT_TOKEN
from chatbot.handler import TelegramBotHandler
from chatbot.answer_generators import LinearSVCAnswerGenerator \
    as AnswerGenerator


class SkillboxTestChatbot:

    def __init__(self):
        self._handler = TelegramBotHandler(BOT_TOKEN)
        self._answer_generator = AnswerGenerator()
        self._new_offset = None
        self._unprocessed_messages = []

    def run(self):
        while True:
            try:
                self._get_updates()
                self._process_messages()
            except Exception as e:
                print(e)

    def _get_updates(self):
        updates = self._handler.get_updates(offset=self._new_offset)
        if updates:
            for message in updates:
                self._unprocessed_messages.append(message)

    def _process_messages(self):
        while self._unprocessed_messages:
            message = self._unprocessed_messages.pop(0)
            self._answer_message(message)

    def _answer_message(self, message):

        message_text = message['message']['text']
        chat_id = message['message']['chat']['id']
        message_id = message['update_id']

        answer = self._answer_generator.get_answer(message_text)
        self._handler.send_message(chat_id, answer)

        self._new_offset = message_id + 1


if __name__ == '__main__':

    try:
        test_chatbot = SkillboxTestChatbot()
        test_chatbot.run()
    except KeyboardInterrupt:
        exit()
