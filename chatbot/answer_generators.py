import random
from abc import ABC, abstractmethod

import nltk
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.svm import LinearSVC

from config import ALLOWED_SYMBOLS, PHRASEBOOK, MAX_LEVENSTEIN_DISTANCE


class AbstractAnswerGenerator(ABC):

    def __init__(self):
        self._phrasebook = PHRASEBOOK
        self._allowed_symbols = ALLOWED_SYMBOLS

    def _normalize_text(self, text):
        text = text.lower()
        text = [char for char in text if char in self._allowed_symbols]
        text = ''.join(text)
        return text

    def _get_failure_phrase(self):
        phrases = self._phrasebook['failure_phrases']
        return random.choice(phrases)

    @abstractmethod
    def _get_intent(self, message):
        raise Exception('Abstract method usage')

    def _get_answer_by_intent(self, message):
        intent = self._get_intent(message)
        if intent:
            phrases = self._phrasebook['intents'][intent]['responses']
            return random.choice(phrases)

    def get_answer(self, message):
        answer = self._get_answer_by_intent(message)
        if answer:
            return answer

        answer = self._get_failure_phrase()
        return answer


class BaseAnswerGenerator(AbstractAnswerGenerator):

    def _get_intent(self, message):
        message = self._normalize_text(message)
        for intent, intent_data in self._phrasebook['intents'].items():
            if message in intent_data['examples']:
                return intent


class LevenshteinAnswerGenerator(BaseAnswerGenerator):

    def __init__(self):
        super(LevenshteinAnswerGenerator, self).__init__()
        self._max_levenshtein_distance = MAX_LEVENSTEIN_DISTANCE

    def _are_close_by_levenshtein_distance(self, example, message):
        if not (example and message):
            return False

        absolute_distance = nltk.edit_distance(example, message)
        if absolute_distance/len(example) < self._max_levenshtein_distance:
            return True

        return False

    def _get_intent(self, message):
        message = self._normalize_text(message)
        for intent, intent_data in self._phrasebook['intents'].items():
            for example in intent_data['examples']:
                example = self._normalize_text(example)
                if self._are_close_by_levenshtein_distance(example, message):
                    return intent


class LinearSVCAnswerGenerator(BaseAnswerGenerator):

    def __init__(self):
        super(LinearSVCAnswerGenerator, self).__init__()
        self._vectorizer = TfidfVectorizer(analyzer='char_wb',
                                           ngram_range=(2, 4))

        phrases = []
        intents = []
        for intent, intent_data in self._phrasebook['intents'].items():
            for example in intent_data['examples']:
                phrases.append(example)
                intents.append(intent)

        phrase_vectors = self._vectorizer.fit_transform(phrases)
        self._classifier = LinearSVC().fit(phrase_vectors, intents)

    def _get_intent(self, message):

        intent = super(LinearSVCAnswerGenerator, self)._get_intent(message)

        if intent:
            return intent
        else:
            message = self._normalize_text(message)
            message_vectorized = self._vectorizer.transform([message])

            intent = self._classifier.predict(message_vectorized)
            if intent:
                intent = intent[0]
                return intent
